function compute_div_free_bases(p)

B = comb2(p);

[H, kH] = hkernel2(B);
DB = divfree2(kH, B);

for i = 1:length(DB)
    disp(['basis ' num2str(i) ':'])
    disp(DB{i})
end

end
