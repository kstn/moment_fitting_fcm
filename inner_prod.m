function f = inner_prod(u, v)
syms x y
f = int(int(u*v, x, -1, 1), y, -1, 1);
end
