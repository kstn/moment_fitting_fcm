function y = q4(i, x, y)
if i == 1
    y = (1-x)*(1-y)/4;
elseif i == 2
    y = (1+x)*(1-y)/4;
elseif i == 3
    y = (1+x)*(1+y)/4;
elseif i == 4
    y = (1-x)*(1+y)/4;
end
end
