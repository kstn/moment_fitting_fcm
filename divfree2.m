%% compute the divergence free basis function given the H-kernel and the orthonormal bases
function DB = divfree2(kH, B)

d = 2;
m = length(B);
n = size(kH, 2);
e = eye(d);

for k = 1:n
    v = kH(:, k);
    DB{k} = 0;
    for i = 1:m
        for j = 1:d
            DB{k} = DB{k} + v((i-1)*d+j)*B(i)*e(:,j);
        end
    end
end

end

