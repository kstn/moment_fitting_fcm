function f = dfp(i, x, y)
if i == 1
    f = [0 0; 0 0];
elseif i == 2
    f = [0 0; 0 0];
elseif i == 3
    f = [0 sqrt(3)/2; 0 0];
elseif i == 4
    f = [sqrt(3)/2 0; 0 -sqrt(3)/2];
elseif i == 5
    f = [0 0; sqrt(3)/2 0];
elseif i == 6
    f = [0 2*y; 0 0]; % TODO
elseif i == 7
    f = [2*y 2*x; 0 -2*y]; % TODO
elseif i == 8
    f = [2*x 0; -2*y -2*x]; % TODO
elseif i == 9
    f = [0 0; 2*x 0]; % TODO
else
    f = [0 0];
end
end

%function f = dfp(i, x, y)
%if i == 1
%    f = [0 0; 0 0];
%elseif i == 2
%    f = [0 0; 0 0];
%elseif i == 3
%    f = [0 0; 1 0];
%elseif i == 4
%    f = [1 0; 0 -1];
%elseif i == 5
%    f = [0 1; 0 0];
%elseif i == 6
%    f = [0 2*y; 0 0];
%elseif i == 7
%    f = [2*y 2*x; 0 -2*y];
%elseif i == 8
%    f = [2*x 0; -2*y -2*x];
%elseif i == 9
%    f = [0 0; 2*x 0];
%else
%    f = [0 0];
%end
%end

