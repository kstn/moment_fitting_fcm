%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% REF: M. Gehrke, Implementierung und Untersuchung numerischer Quadraturverfahren für diskontinuierliche Integranden in der Finiten-Zell-Methode
%% Example 1, sec 4.1; Testfall 1, sec 6.1
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function test2

close all

clear

x_min = -1.0;
x_max = 1.0;

y_min = -1.0;
y_max = 1.0;

nx = 1;
ny = 1;

dx = (x_max - x_min) / nx;
dy = (y_max - y_min) / ny;

params.a = 4;
params.b = -1;
params.c = -2;

p = 1; % surface integration order

q = 2; % divergence-free order

r = 4; % line integration order

stats = zeros(nx, ny); % status array (cut|no cut)
quad = cell(nx, ny); % quadrature container
cut_cells = 0;
inner_cells = 0;
surface_integral = 0;
for i = 1:nx
    for j = 1:ny
        % 4 corners of the cell
        V = [x_min+(i-1)*dx  y_min+(j-1)*dy;
             x_min+i*dx  y_min+(j-1)*dy;
             x_min+i*dx  y_min+j*dy;
             x_min+(i-1)*dx  y_min+j*dy];

        % compute the level set of each vertex
        nn = size(V, 1);
        d = zeros(1, nn);
        for n = 1:nn
            phi(n) = Phi(V(n, 1), V(n, 2), params);
        end

        % check if the cell is cut by the level set
        in = find(phi<=0);
        out = find(phi>0);

        if(length(in) == nn)
            stats(i, j) = 0; % 0: in, 1: out, -1: cut
        else
            if(length(out) == nn)
                stats(i, j) = 1;
            else
                stats(i, j) = -1;
            end
        end

        if(stats(i, j) == -1)
            disp(['cell ', num2str(i), ', ', num2str(j), ' is cut, lower left=', num2str(V(1,:))]);
            cut_cells = cut_cells + 1;
%            V

            % for cut cell we have to compute the quadrature
            if q == 0
                K = 2;
            elseif q == 1
                K = 5;
            elseif q == 2
                K = 9;
            end

            [W, Q] = quadrature(p, 'GAUSS', 2);
            N = length(W);

%            %% test the divergence free basis
%            divfp = zeros(1, K);
%            for k = 1:K
%                for ip = 1:N
%                    x = 0.0;
%                    y = 0.0;
%                    dxdxi = [0.0 0.0];
%                    dydxi = [0.0 0.0];
%                    for s = 1:4
%                        x = x+q4(s, Q(ip, 1), Q(ip, 2)) * V(s, 1);
%                        y = y+q4(s, Q(ip, 1), Q(ip, 2)) * V(s, 2);
%                        dxdxi = dxdxi + dq4(s, Q(ip, 1), Q(ip, 2)) * V(s, 1);
%                        dydxi = dydxi + dq4(s, Q(ip, 1), Q(ip, 2)) * V(s, 2);
%                    end
%                    jac = [dxdxi;dydxi]';
%                    aux = dfp(k, Q(ip, 1), Q(ip, 2));
%                    dfpx = jac\aux(1,:)';
%                    dfpy = jac\aux(2,:)';
%                    divfp(k) = divfp(k) + (dfpx(1) + dfpy(2)) * W(ip) * det(jac);
%                end
%            end
%%            divfp
%            if norm(divfp) > 1e-10
%                disp(['the divergence-free basis is non-zero']);
%            end

            A = zeros(K, N);
            for ip = 1:N
                aux = fp(q, Q(ip, 1), Q(ip, 2));
                for k = 1:K
                    x = 0.0; y = 0.0;
                    for s = 1:4
                        x = x+q4(s, Q(ip, 1), Q(ip, 2)) * V(s, 1);
                        y = y+q4(s, Q(ip, 1), Q(ip, 2)) * V(s, 2);
                    end
                    dphi = dPhi(x, y, params);
                    ni = dphi / norm(dphi);
%                    x
%                    y
%                    ni
                    A(k, ip) = dot(aux(k,:), ni);
                end
            end

            b = zeros(K, 1);
            % identify the cut part
            normal = [0 -1; 1 0; 0 1; -1 0];
            locvertex = [-1 -1; 1 -1; 1 1; -1 1];
            [W1,Q1] = quadrature(r, 'GAUSS', 1);
            for e = 1:4
                v1 = e;
                if e==4
                    v2 = 1;
                else
                    v2 = e+1;
                end

                % handle the cut edge
                if phi(v1)*phi(v2) < 0
                    inte = bisect(@Phi, V(v1,:), V(v2,:), params, 1e-10);
                    if phi(v1) < 0
                        vi = v1;
                    else
                        vi = v2;
                    end
%                    inte
%                    V(vi,:)
                    leng = norm(inte-V(vi,:));
                    locx = (inte(1)-V(1,1)) / (V(2,1)-V(1,1))*2 - 1;
                    locy = (inte(2)-V(2,2)) / (V(3,2)-V(2,2))*2 - 1;
                    for ip = 1:length(W1)
                        locqx = (locx-locvertex(vi,1))/2*Q1(ip) + (locx+locvertex(vi,1))/2;
                        locqy = (locy-locvertex(vi,2))/2*Q1(ip) + (locy+locvertex(vi,2))/2;
%                        locqx
%                        locqy
%                        normal(e,:)
                        aux = fp(q, locqx, locqy);
                        for k = 1:K
                            b(k) = b(k) - W1(ip)*dot(aux(k,:), normal(e,:))*leng*0.5;
                        end
                    end
                    disp(['edge ', num2str(v1), ', ', num2str(v2), ' is cut, leng=', num2str(leng), ', n=', num2str(normal(e,:)),', integ=', num2str(inte), ', intel=', num2str([locx locy])]);
                end

                % handle the inner edge
                if phi(v1) < 0 && phi(v2) < 0
                    leng = norm(V(v2,:)-V(v1,:));
                    for ip = 1:length(W1)
                        locqx = (locvertex(v2,1)-locvertex(v1,1))/2*Q1(ip) + (locvertex(v2,1)+locvertex(v1,1))/2;
                        locqy = (locvertex(v2,2)-locvertex(v1,2))/2*Q1(ip) + (locvertex(v2,2)+locvertex(v1,2))/2;
%                        locqx
%                        locqy
                        aux = fp(q, locqx, locqy);
                        for k = 1:K
                            b(k) = b(k) - W1(ip)*dot(aux(k, :), normal(e,:))*leng*0.5;
                        end
                    end
                    disp(['edge ', num2str(v1), ', ', num2str(v2), ' is inner, leng=', num2str(leng), ', n=', num2str(normal(e,:))]);
                end
            end

%            A
%            b
%            w = A'*((A*A')\b);
            w = A\b;
%            w
        elseif stats(i, j) == 0
            disp(['cell ', num2str(i), ', ', num2str(j), ' is inner, lower left=', num2str(V(1,:))]);
            inner_cells = inner_cells + 1;
            w = W;
        end

        if stats(i, j) == 0 || stats(i, j) == -1
            area = 0;
            cell_surface_integral = 0.0;
            for ip = 1:N
                x = 0.0;
                y = 0.0;
                dxdxi = [0.0 0.0];
                dydxi = [0.0 0.0];
                for s = 1:4
                    x = x+q4(s, Q(ip, 1), Q(ip, 2)) * V(s, 1);
                    y = y+q4(s, Q(ip, 1), Q(ip, 2)) * V(s, 2);
                    dxdxi = dxdxi + dq4(s, Q(ip, 1), Q(ip, 2)) * V(s, 1);
                    dydxi = dydxi + dq4(s, Q(ip, 1), Q(ip, 2)) * V(s, 2);
                end
                jac = det([dxdxi;dydxi]);
%                x
%                y
%                jac
                cell_surface_integral = cell_surface_integral + func(x, y)*jac*w(ip);
                area = area + jac*w(ip);
            end
            surface_integral = surface_integral + cell_surface_integral;
            disp(['cell ', num2str(i), ', ', num2str(j), ' inner area: ', num2str(area)]);
            disp(['cell ', num2str(i), ', ', num2str(j), ' cell_surface_integral: ', num2str(cell_surface_integral)]);
            disp(['-------------end cell ', num2str(i), ', ', num2str(j), '-------------------------']);
        end
    end
end
disp(['number of cut cells: ', num2str(cut_cells)]);
disp(['number of inner cells: ', num2str(inner_cells)]);
disp(['total number of cells: ', num2str(nx*ny)]);
disp(['surface_integral: ', num2str(surface_integral, 16)]);
end

function y = func(c, y) % function to be integrated
y = 1.0;
end

function f = Phi(x, y, params)
    a = params.a;
    b = params.b;
    c = params.c;
    f = a*x + b*y + c;
end

function df = dPhi(x, y, params)
    a = params.a;
    b = params.b;
    df = [a b];
end

