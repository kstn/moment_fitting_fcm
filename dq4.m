function y = dq4(i, x, y)
if i == 1
    y = [-(1-y)/4 -(1-x)/4];
elseif i == 2
    y = [(1-y)/4 -(1+x)/4];
elseif i == 3
    y = [(1+y)/4 (1+x)/4];
elseif i == 4
    y = [-(1+y)/4 (1-x)/4];
end
end
