%% compute the intersection of the level set with a line connect by 2 points
function P = bisect(Phi, P1, P2, params, tol)
    f1 = Phi(P1(1), P1(2), params);
    f2 = Phi(P2(1), P2(2), params);

    left = 0.0;
    right = 1.0;

    converged = 0;
    while converged==0
        mid = (left+right)/2;
        P = P1 + mid*(P2-P1);
        fm = Phi(P(1), P(2), params);
        if fm*f1<0
            right = mid;
        else
            left = mid;
        end

        if (right-left) < tol
            converged = 1;
        end
    end
end
