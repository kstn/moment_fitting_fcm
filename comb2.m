%% compute the orthonormal monomial basis upto degree p for 2d (variables: x, y)
function B = comb2(p)

syms x y

%% create an array of non-normalized monomials
if p == 0
    Bnn = [1];
elseif p == 1
    Bnn = [1 x y];
elseif p == 2
    Bnn = [1 x y x^2 x*y y^2];
elseif p == 3
    Bnn = [1 x y x^2 x*y y^2 x^3 x^2*y x*y^2 y^3];
elseif p == 4
    Bnn = [1 x y x^2 x*y y^2 x^3 x^2*y x*y^2 y^3 x^4 x^3*y x^2*y^2 x*y^3 y^4];
else
    error(['degree ', num2str(p), ' is not supported'])
end

%% orthonormalize using Gram-Schmidt
B(1) = Bnn(1)/sqrt(inner_prod(Bnn(1), Bnn(1)));
for i = 2:length(Bnn)
    B(i) = Bnn(i);
    for j = 1:i-1
        B(i) = B(i) - inner_prod(B(i), B(j))*B(j);
    end
    B(i) = B(i)/sqrt(inner_prod(B(i), B(i)));
end

end

