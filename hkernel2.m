%% given the orthonormal basis B, compute the H kernel
%% REF: M. Gehrke Msc thesis, p. 20
function [H, kH] = hkernel2(B)

syms x y

d = 2;
m = length(B);
H = zeros(m, m*d);

for i = 1:m
    for j = 1:d
        if j == 1
            div = diff(B(i), x);
        elseif j == 2
            div = diff(B(i), y);
        end

        for k=1:m
            cik = inner_prod(div, B(k));
            H(k, d*(i-1)+j) = cik;
        end
    end
end

kH = null(H);

end

